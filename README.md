# Field Validator #

#### Description ####

A simple iOS project written in Objective-C demonstrating a how to format the input typed into a text field. The text field is a subclass of UITextField that has a "validator" property. The validator is a custom class that formats and validates the text entered by the user. 

The example validator implemented in the project formats and validates North American phone numbers, similar to what you find in Apple's "Contacts" application. 

#### Author ####

Mario Diana

#### License ####

The MIT License
