//
//  ViewController.m
//  FieldValidator
//
//  Created by Mario Diana on 6/24/18.
//  Copyright © 2018 Mario Diana. All rights reserved.
//

#import "ViewController.h"
#import "MDXTextField.h"
#import "MDXPhoneFieldValidator.h"

@interface ViewController ()
@property (weak, nonatomic) IBOutlet MDXTextField *phoneField;
@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    id validator = [[MDXPhoneFieldValidator alloc] init];
    
    // There are two steps to setting up the validator.
    [validator startReceivingNotificationsFromTextField:[self phoneField]];
    self.phoneField.validator = validator;
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
