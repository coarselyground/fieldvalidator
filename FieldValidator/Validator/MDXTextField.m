//
//  MDXTextField.m
//  PhoneNumber
//
//  Created by Mario Diana on 6/24/18.
//  Copyright © 2018 Mario Diana. All rights reserved.
//

#import "MDXTextField.h"
#import "MDXTextFieldValidator.h"


@implementation MDXTextField

- (void)dealloc
{
    _validator = nil;
}


- (NSString *)unformattedText
{
    if ([self validator] && [[self validator] respondsToSelector:@selector(stripFormattingFromUIText:)]) {
        return [[self validator] stripFormattingFromUIText:[super text]];
    }
    else {
        return [super text];
    }
}

@end


@implementation UITextField (MDXValidator)

- (id)mdx_validator
{
    if (![self respondsToSelector:@selector(validator)]) {
        return nil;
    }
    else {
        return [self performSelector:@selector(validator)];
    }
}


- (BOOL)mdx_hasValidatorCapableOfHandlingMessage:(SEL)message
{
    return [[self mdx_validator] respondsToSelector:message];
}


- (BOOL)mdx_isEmpty
{
    NSString* s = [[self text] stringByReplacingOccurrencesOfString:@"\\s"
                                                         withString:@""
                                                            options:NSRegularExpressionSearch
                                                              range:NSMakeRange(0, [[self text] length])];
    
    return [s length] == 0;
}


- (BOOL)mdx_hasValidContents
{
    if ([self mdx_hasValidatorCapableOfHandlingMessage:@selector(textFieldContentIsValid:)]) {
        id validator = [self mdx_validator];
        return [validator textFieldContentIsValid:self];
    }
    else {
        // By default, any input is valid.
        return YES;
    }
}

@end
