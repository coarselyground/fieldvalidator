//
//  MDXTextField.h
//  PhoneNumber
//
//  Created by Mario Diana on 6/24/18.
//  Copyright © 2018 Mario Diana. All rights reserved.
//

#import <UIKit/UIKit.h>

/**
 * Simple subclass of UITextField allowing for custom validation.
 */
@interface MDXTextField : UITextField

/** Provides custom validation of text field content. */
@property (nonatomic, strong) id validator;

/** Return text in field, stripped by validator of any formatting characters. */
- (NSString *)unformattedText;

@end


/**
 * Category facilitating use of validator from within UITextFieldDelegate methods.
 */
@interface UITextField (MDXValidator)

/** Return validator or nil. */
- (id)mdx_validator;

/** Return YES if text field has validator which can handle the message; NO, otherwise. */
- (BOOL)mdx_hasValidatorCapableOfHandlingMessage:(SEL)message;

/** Return YES if text in field is an empty string; NO, otherwise. */
- (BOOL)mdx_isEmpty;

/** Return YES for contents conforming to custom validation; or, YES by default. */
- (BOOL)mdx_hasValidContents;

@end
