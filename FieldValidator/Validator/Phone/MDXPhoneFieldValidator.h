//
//  MDXPhoneFieldValidator.h
//  PhoneNumber
//
//  Created by Mario Diana on 6/24/18.
//  Copyright © 2018 Mario Diana. All rights reserved.
//

#import "MDXTextFieldValidator.h"

@class UITextField;

/**
 * Provide validation for North American telephone numbers.
 */
@interface MDXPhoneFieldValidator : NSObject <MDXTextfieldValidator>

- (NSString *)stripFormattingFromUIText:(NSString *)text;
- (void)startReceivingNotificationsFromTextField:(UITextField *)textField;
- (void)stopReceivingNotifications;

@end
