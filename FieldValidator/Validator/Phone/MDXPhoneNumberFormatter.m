//
//  MDXPhoneNumberFormatter.m
//  PhoneNumber
//
//  Created by Mario Diana on 6/24/18.
//  Copyright © 2018 Mario Diana. All rights reserved.
//

#import "MDXPhoneNumberFormatter.h"

const NSUInteger MDXNorthAmericanTelephoneNumberLength = 10;

static const NSUInteger kPhoneNumberExchangeThreshold = 3;
static const NSUInteger kPhoneNumberAreaCodeThreshold = 7;

@interface MDXPhoneNumberFormatter ()
@property (nonatomic, strong) NSCharacterSet* numberFilter;
@end

@implementation MDXPhoneNumberFormatter

#pragma mark - Object lifecycle

+ (MDXPhoneNumberFormatter *)sharedInstance
{
    static MDXPhoneNumberFormatter* instance;
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        instance = [[MDXPhoneNumberFormatter alloc] initSingleton];
    });
    
    return instance;
}


- (void)dealloc
{
    _numberFilter = nil;
}


- (instancetype)initSingleton
{
    self = [super init];
    
    if (self) {
        _numberFilter = [[NSCharacterSet characterSetWithCharactersInString:@"0123456789"] invertedSet];
    }
    
    return self;
}


#pragma mark - Public methods

- (NSString *)stripFormattingFromPhoneNumber:(NSString *)phoneNumber
{
    // Characters used to format area code and exchange prefix.
    NSCharacterSet* separators = [NSCharacterSet characterSetWithCharactersInString:@")(- "];
    NSArray* components = [phoneNumber componentsSeparatedByCharactersInSet:separators];
    return [components componentsJoinedByString:@""];
}


- (NSString *)formatPhoneNumber:(NSString *)phoneNumber
{
    // Order of evaluation is critical!
    if ([phoneNumber length] > kPhoneNumberAreaCodeThreshold) {
        return [self applyAreaCodeFormatToPhoneNumber:phoneNumber];
    }
    else if ([phoneNumber length] > kPhoneNumberExchangeThreshold) {
        return [self applyExchangeFormatToPhoneNumber:phoneNumber];
    }
    else {
        return phoneNumber;
    }
}


#pragma mark - Private methods

- (BOOL)isValidInput:(id)input
{
    if (![input isKindOfClass:[NSString class]]) {
        return NO;
    }
    
    NSRange range = [input rangeOfCharacterFromSet:[self numberFilter]];
    BOOL valid = range.location == NSNotFound;
    
    if (valid) {
        valid = [input length] <= MDXNorthAmericanTelephoneNumberLength;
    }
    
    return valid;
}


- (NSString *)applyExchangeFormatToPhoneNumber:(NSString *)phoneNumber
{
    NSString* formattedNumber = [NSString stringWithFormat:@"%@-%@",
                                 [phoneNumber substringWithRange:NSMakeRange(0, 3)],
                                 [phoneNumber substringWithRange:NSMakeRange(3, [phoneNumber length] - 3)]];
    
    return formattedNumber;
}


- (NSString *)applyAreaCodeFormatToPhoneNumber:(NSString *)phoneNumber
{
    NSString* formattedNumber = [NSString stringWithFormat:@"(%@) %@-%@",
                                 [phoneNumber substringWithRange:NSMakeRange(0, 3)],
                                 [phoneNumber substringWithRange:NSMakeRange(3, 3)],
                                 [phoneNumber substringWithRange:NSMakeRange(6, [phoneNumber length] - 6)]];
    
    return formattedNumber;
}


#pragma mark - Superclass methods

/**
 * Return formatted string representing input object.
 */
- (NSString *)stringForObjectValue:(id)obj
{
    if (![self isValidInput:obj]) {
        return nil;
    }
    
    NSString* phone = (NSString *)obj;
    NSString* formattedPhoneNumber = [self formatPhoneNumber:phone];
    
    return formattedPhoneNumber;
}

/**
 * Return YES if (formatted) string is successfully stripped of formatting.
 */
- (BOOL)getObjectValue:(out id  _Nullable *)obj
             forString:(NSString *)string
      errorDescription:(out NSString * _Nullable *)error
{
    if (string) {
        *obj = [self stripFormattingFromPhoneNumber:string];
        return YES;
    }
    else {
        if (error) {
            *error = @"Argument forString contained nil value.";
        }
        
        return NO;
    }
}

@end
