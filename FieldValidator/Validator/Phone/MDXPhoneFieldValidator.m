//
//  MDXPhoneFieldValidator.m
//  PhoneNumber
//
//  Created by Mario Diana on 6/24/18.
//  Copyright © 2018 Mario Diana. All rights reserved.
//

#import "MDXPhoneFieldValidator.h"

#import <UIKit/UIKit.h>
#import "MDXPhoneNumberFormatter.h"


// Adapted from: https://stackoverflow.com/a/40272772/155167
@interface UITextField (MDXCursorPosition)

- (NSInteger)mdx_cursorPosition;
- (void)mdx_setCursorPosition:(NSInteger)position;
- (void)mdx_changeText:(NSString *)text cursorPosition:(NSInteger)cursor;

@end

@implementation UITextField (MDXCursorPosition)

- (NSInteger)mdx_cursorPosition
{
    UITextRange* selectedRange = self.selectedTextRange;
    UITextPosition* textPosition = selectedRange.start;
    return [self offsetFromPosition:self.beginningOfDocument toPosition:textPosition];
}


- (void)mdx_setCursorPosition:(NSInteger)position
{
    UITextPosition* textPosition = [self positionFromPosition:self.beginningOfDocument offset:position];
    [self setSelectedTextRange:[self textRangeFromPosition:textPosition toPosition:textPosition]];
}


- (void)mdx_changeText:(NSString *)text cursorPosition:(NSInteger)cursor
{
    // Simulate what happens when the user changes text in a text field.
    [self setText:text];
    [self mdx_setCursorPosition:cursor];
    
    NSNotification* notification = [[NSNotification alloc] initWithName:UITextFieldTextDidChangeNotification
                                                                 object:self
                                                               userInfo:nil];
    
    [[NSNotificationCenter defaultCenter] postNotification:notification];
}

@end


@interface MDXPhoneFieldValidator ()
@property (nonatomic, strong) NSCharacterSet* filter;
@end

@implementation MDXPhoneFieldValidator

#pragma mark - Object lifecycle

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    _filter = nil;
}

- (instancetype)init
{
    self = [super init];
    
    if (self) {
        _filter = [NSCharacterSet characterSetWithCharactersInString:@"0123456789"];
    }
    
    return self;
}


#pragma mark - Public methods

- (NSString *)stripFormattingFromUIText:(NSString *)text
{
    return [self cleanFormattedPhoneNumber:text];
}


- (void)startReceivingNotificationsFromTextField:(UITextField *)textField
{
    NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
    [nc removeObserver:self];
    
    if (textField) {
        // Setup all the notifications we're observing from the text field.
        [nc addObserver:self selector:@selector(textFieldDidChange:)
                   name:UITextFieldTextDidChangeNotification
                 object:textField];
    }
}


- (void)stopReceivingNotifications
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}


#pragma mark - Protocol implementation methods

- (BOOL)textFieldContentIsValid:(UITextField *)textField
{
    NSString* phoneNumber = [[MDXPhoneNumberFormatter sharedInstance] stripFormattingFromPhoneNumber:[textField text]];
    return [phoneNumber length] == MDXNorthAmericanTelephoneNumberLength;
}


#pragma mark - UITextFieldDelegate methods and notifications

- (void)textFieldDidChange:(NSNotification *)notification
{
    id textField = [notification object];
    
    NSString* rawString = [textField text];
    NSString* cleanString = [self cleanFormattedPhoneNumber:rawString];
    NSString* formattedString = [self formatCleanPhoneNumber:cleanString];
    
    NSInteger cursor = [textField mdx_cursorPosition];
    cursor = [self calculateCursorPosition:cursor forFormattedString:formattedString withRawString:rawString];
    
    [textField setText:formattedString];
    [textField mdx_setCursorPosition:cursor];
}


- (BOOL)textField:(UITextField *)textField
shouldChangeCharactersInRange:(NSRange)range
replacementString:(NSString *)string
{
    // A DELETE is characterized by an empty replacement string.
    if ([@"" isEqualToString:string]) {
        return [self textField:textField shouldDeleteCharactersInRange:range];
    }
    else {
        // A North American phone number is 10 digits.
        return ([self isNumberString:string] && [self textFieldCanAcceptInput:textField]);
    }
}


#pragma mark - Utility methods

- (NSString *)formatCleanPhoneNumber:(NSString *)phoneNumber
{
    return [[MDXPhoneNumberFormatter sharedInstance] stringForObjectValue:phoneNumber];
}


- (NSString *)cleanFormattedPhoneNumber:(NSString *)phoneNumber
{
    NSString* cleanString = nil;
    NSString* errorDescription = nil;
    
    [[MDXPhoneNumberFormatter sharedInstance] getObjectValue:&cleanString
                                                  forString:phoneNumber
                                           errorDescription:&errorDescription];
    
    if (errorDescription) {
        NSException* ex = [NSException exceptionWithName:@"MDXCleaningFormattedStringException"
                                                  reason:errorDescription
                                                userInfo:nil];
        
        [ex raise];
    }
    
    return cleanString;
}


- (BOOL)isNumberString:(NSString *)string
{
    return [[self filter] isSupersetOfSet:[NSCharacterSet characterSetWithCharactersInString:string]];
}


- (BOOL)isNumberUnichar:(unichar)character
{
    return [[self filter] characterIsMember:character];
}


- (BOOL)textFieldCanAcceptInput:(UITextField *)textField
{
    NSString* phoneNumber = [[MDXPhoneNumberFormatter sharedInstance] stripFormattingFromPhoneNumber:[textField text]];
    return [phoneNumber length] < MDXNorthAmericanTelephoneNumberLength;
}


- (BOOL)textField:(UITextField *)textField shouldDeleteCharactersInRange:(NSRange)range
{
    NSString* deletedCharacter = [[textField text] substringWithRange:range];
    
    if ([self isNumberString:deletedCharacter]) {
        return YES;
    }
    else if ([@"(" isEqualToString:deletedCharacter]) {
        // Do what Apple does.
        [textField mdx_setCursorPosition:[[textField text] length]];
        return NO;
    }
    else {
        NSInteger cursorOffset = 0;
        
        // Manually delete the first digit to the left of the special character.
        while (![self isNumberString:[[textField text] substringWithRange:range]] && range.location > 0) {
            cursorOffset += 1;
            range.location = range.location - 1;
        }
        
        NSMutableString* replacement = [NSMutableString stringWithString:[textField text]];
        [replacement deleteCharactersInRange:range];
        
        NSInteger cursor = [textField mdx_cursorPosition] - cursorOffset;
        [textField mdx_changeText:replacement cursorPosition:cursor];
        
        return NO;
    }
}


#pragma mark - Cursor adjustment methods

- (NSInteger)resetCursor:(NSInteger)cursor usingRawString:(NSString *)string
{
    NSUInteger size = [string length];
    unichar buffer[size + 1];
    
    [string getCharacters:buffer range:NSMakeRange(0, size)];
    NSUInteger count = 0;
    
    for(int i = 0; i < cursor; i++) {
        if (![self isNumberUnichar:buffer[i]]) {
            count += 1;
        }
    }
    
    cursor -= count;
    
    return cursor;
}


- (NSInteger)adjustCursor:(NSInteger)cursor forFormattedString:(NSString *)string
{
    NSUInteger size = [string length];
    unichar buffer[size + 1];
    
    [string getCharacters:buffer range:NSMakeRange(0, size)];
    
    for (int i = 0; i < size; i++) {
        if (i == cursor) {
            // We must not go further, or the cursor will be off.
            break;
        }
        
        if (![self isNumberUnichar:buffer[i]]) {
            cursor += 1;
        }
    }
    
    return cursor;
}


- (NSInteger)calculateCursorPosition:(NSInteger)cursor
                  forFormattedString:(NSString *)formattedString
                       withRawString:(NSString *)rawString
{
    if (cursor == 0 || cursor == [formattedString length]) {
        // Optimization.
        return cursor;
    }
    
    cursor = [self resetCursor:cursor usingRawString:rawString];
    
    return [self adjustCursor:cursor forFormattedString:formattedString];
}

@end
