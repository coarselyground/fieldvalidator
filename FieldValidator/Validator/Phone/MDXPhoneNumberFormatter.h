//
//  MDXPhoneNumberFormatter.h
//  PhoneNumber
//
//  Created by Mario Diana on 6/24/18.
//  Copyright © 2018 Mario Diana. All rights reserved.
//

#import <Foundation/Foundation.h>

extern const NSUInteger MDXNorthAmericanTelephoneNumberLength;

/**
 * Provide formatting of North American telephone numbers.
 */
@interface MDXPhoneNumberFormatter : NSFormatter

/** Return singleton. */
+ (MDXPhoneNumberFormatter *)sharedInstance;

// init methods unavailable.
- (instancetype)init NS_UNAVAILABLE;
- (instancetype)new NS_UNAVAILABLE;

- (NSString *)formatPhoneNumber:(NSString *)phoneNumber;
- (NSString *)stripFormattingFromPhoneNumber:(NSString *)phoneNumber;

@end
