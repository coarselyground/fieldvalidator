//
//  MDXTextFieldValidator.h
//  PhoneNumber
//
//  Created by Mario Diana on 6/24/18.
//  Copyright © 2018 Mario Diana. All rights reserved.
//

#import <Foundation/Foundation.h>

@class UITextField;

/**
 * Protocol providing custom validation of text field content.
 */
@protocol MDXTextfieldValidator <NSObject>

/** Return YES if text content of text field is valid; NO, otherwise. */
- (BOOL)textFieldContentIsValid:(UITextField *)textField;

@optional
/** Returns unformatted text. */
- (NSString *)stripFormattingFromUIText:(NSString *)text;

- (void)startReceivingNotificationsFromTextField:(UITextField *)textField;
- (void)stopReceivingNotifications;

@end
