//
//  AppDelegate.h
//  FieldValidator
//
//  Created by Mario Diana on 6/24/18.
//  Copyright © 2018 Mario Diana. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
